<?php
/**
 * @file
 * Breadcrumb Manager administration.
 */

/**
 * Breadcrumb Manager Settings form.
 */
function breadcrumb_manager_settings_form($form, &$form_state) {

  $form['breadcrumb_manager_excluded_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Excluded paths'),
    '#default_value' => variable_get('breadcrumb_manager_excluded_paths', "search/*"),
    '#description' => t('Enter a list of path that will not be affected by Breadcrumb Manager. You can use "*" as wildcard.'),
    '#rows' => 5,
  );

  $form['breadcrumb_manager_show_front'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show on front page'),
    '#default_value' => variable_get('breadcrumb_manager_show_front', FALSE),
    '#description' => t('If checked, the breadcrumb will be shown even on front page.'),
  );

  $form['breadcrumb_manager_show_home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show "Home" breadcrumb link'),
    '#default_value' => variable_get('breadcrumb_manager_show_home', TRUE),
    '#description' => t('Uncheck this option in order to omit Home link from breadcrumb. If you cannot see it even with this option, please check your frontend theme settings.'),
  );

  $form['breadcrumb_manager_home'] = array(
    '#type' => 'textfield',
    '#title' => 'Override "Home" label',
    '#default_value' => variable_get('breadcrumb_manager_home', ''),
    '#attributes' => array(
      'placeholder' => 'Home',
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="breadcrumb_manager_show_home"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t('Enter text that will override default "Home" label link. Leave it empty to use "Home".'),
  );

  $form['breadcrumb_manager_show_current'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show current page title at end'),
    '#default_value' => variable_get('breadcrumb_manager_show_current', TRUE),
    '#description' => t('Uncheck this option in order to omit current page link from breadcrumb. If you cannot see it even with this option, please check your frontend theme settings.'),
  );

  $form['breadcrumb_manager_show_current_as_link'] = array(
    '#type' => 'checkbox',
    '#title' => 'Display current page title as link',
    '#default_value' => variable_get('breadcrumb_manager_show_current_as_link', TRUE),
    '#states' => array(
      'visible' => array(
        ':input[name="breadcrumb_manager_show_current"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t('Check this option to display last item as a link. Otherwise it will be shown as plain text.'),
  );

  $form['breadcrumb_manager_set_active_trail'] = array(
    '#type' => 'checkbox',
    '#title' => t('Set active trail'),
    '#default_value' => variable_get('breadcrumb_manager_set_active_trail', FALSE),
    '#description' => t('If checked, Breadcrumb Manager will set the active trail on the Main Menu'),
  );

  $form['breadcrumb_manager_set_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Set page title'),
    '#default_value' => variable_get('breadcrumb_manager_set_title', FALSE),
    '#description' => t('If checked, Breadcrumb Manager will set the current page title if does not exists.'),
  );

  $form['breadcrumb_manager_titles_blacklist'] = array(
    '#type' => 'textarea',
    '#title' => t('Page titles blacklist'),
    '#default_value' => variable_get('breadcrumb_manager_titles_blacklist', ''),
    '#description' => t('Enter a list of titles (one title per row), for which the page title must be overwritten by the computed one.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="breadcrumb_manager_set_title"]' => array('checked' => FALSE),
      ),
    ),
  );

  return system_settings_form($form);
}
